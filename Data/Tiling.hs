{- |
Module      :  Data.Tiling
Copyright   :  (c) Claude Heiland-Allen 2011
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Substitution tilings.  The term substitution, in connection with tilings,
describes a simple but powerful method to produce tilings with many
interesting properties.
 
The main idea is to use a finite set of building blocks called prototiles,
an expanding linear map (the inflation factor), and a rule how to dissect
each scaled tile into copies of the original prototiles.

For some examples of substitution tilings, and a glossary of terminology,
see the /tilings encyclopedia/
at <http://tilings.math.uni-bielefeld.de/>
-}
module Data.Tiling
  ( module Data.Tiling.Class
  , module Data.Tiling.Quad
  ) where

import Data.Tiling.Class
import Data.Tiling.Quad
